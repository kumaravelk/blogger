<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'description', 'banner_image'];

    /**
     * Get the banner image url for this post.
     *
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function bannerImageUrl()
    {
        $path = getPostBannerConfig('link_path') . DIRECTORY_SEPARATOR . $this->banner_image;
        return url($path);
    }

    /**
     * Show page url for this post.
     *
     * @return string
     */
    public function showPageUrl()
    {
        return route('posts.show', ['post' => $this->id]);
    }

    /**
     * Post belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Post has many comments.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
