<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'description' => $this->description,
            'banner_image_url' => $this->bannerImageUrl(),
            'created_at' => $this->created_at->diffForHumans(),
            'user' => new User($this->user),
            'show_page_url' => $this->showPageUrl(),
        ];
    }
}
