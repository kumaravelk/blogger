<?php

use Illuminate\Support\Facades\Log;

/**
 * Common error log method for logging errors with a custom format.
 *
 * @param $message
 * @param $location
 * @param null $errorObject
 * @param array $reference
 */
function logError($message, $location, $errorObject = null, $reference = [])
{
    $data = [
        'location' => $location,
    ];

    if ($errorObject) {
        $data['message'] = $errorObject->getMessage();
        $data['trace'] = $errorObject->getTraceAsString();
    }

    if ($reference) {
        $data['reference'] = $reference;
    }

    Log::error([$message => $data]);
}

/**
 * Get the config env for the given key.
 *
 * @param $key
 * @return \Illuminate\Config\Repository|mixed
 */
function configEnv($key)
{
    return config('env.' . $key);
}

/**
 * Get the config env details of the post banner for the given key.
 *
 * @param $key
 * @return \Illuminate\Config\Repository|mixed
 */
function getPostBannerConfig($key)
{
    return configEnv('post_banner.' . $key);
}

/**
 * Mandatory icon for form input label.
 *
 * @return string
 */
function mandatory()
{
    return '<span class="text-mandatory">*</span>';
}
