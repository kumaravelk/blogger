<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\PostCreateRequest;
use App\Http\Resources\Post as PostResource;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class PostController extends Controller
{
    /**
     * Get the paginated post details for feeds.
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $posts = Post::with('user')->latest()->paginate(10);
        return PostResource::collection($posts);
    }

    /**
     * Create page for creating a post.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     *  Store a post.
     *
     * @param PostCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(PostCreateRequest $request)
    {
        try {
            DB::beginTransaction();
            $input = $request->only('title', 'description');
            $extension = $request->file('banner_image')->getClientOriginalExtension();
            $fileName = 'image' . time() . random_int(1111, 9999) . '.' . $extension;
            $request->file('banner_image')->storeAs(getPostBannerConfig('storage_path'), $fileName);
            $input['banner_image'] = $fileName;
            auth()->user()->posts()->create($input);
            DB::commit();

            return redirect()->route('home')->withStatus('Your post has been published!');
        } catch (Throwable $throwable) {
            DB::rollBack();
            logError('Error occurred while storing a post', 'PostController@store', $throwable, [
                'input' => $request->all(),
            ]);
            return redirect()->back()->withInput()->withError('Unable to post now. Please try again later!');
        }
    }

    /**
     * Show the requested post.
     *
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        $post->load('comments.user');
        $comments = ['base' => []];
        if ($post->comments->isNotEmpty()) {
            $comments = $post->comments->groupBy('parent_id');
            $comments['base'] = $comments[''];
            unset($comments['']);
        }

        return view('posts.show', compact('post', 'comments'));
    }

    /**
     * Add comment for the requested post.
     *
     * @param Post $post
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addComment(Post $post, Request $request)
    {
        if (!$request->input('comment')) {
            return redirect()->route('posts.show', $post)->withError('Please type some comment to reply!');
        }

        $comment = new Comment($request->only('comment', 'parent_id'));
        $comment->user_id = auth()->id();
        $post->comments()->save($comment);
        return redirect()->route('posts.show', $post)->withStatus('Comment added successfully!');
    }
}
