@extends('layouts.app')

@section('title', 'Feeds')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8" id="dashboard">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <div id="feeds">
                    {{-- Feeds goes here..! --}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        let initial = true;
        let getPostUrl = '{{ route('posts.index') }}' + '?page=1';

        $(document).ready(function(){
            loadPosts();
        });


        function loadPosts() {
            if (getPostUrl == null) {
                $('#load-more').html('<p class="text-muted">You caught up everything..!</p>');
                return;
            }
            $.ajax({
                type: 'GET',
                url: getPostUrl,
                success: function (data) {
                    let responseData = JSON.parse(JSON.stringify(data));
                    $.each(responseData.data, function (key, post) {
                        $('#feeds').append(
                            `<div class="card mb-4">
                                <a href="${post.show_page_url}" class="card-link text-black-primary">
                                    <img class="card-img-top" src="${post.banner_image_url}" alt="Post banner image">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title"><a href="${post.show_page_url}" class="card-link text-black-primary">${post.title}</a></h5>
                                    <h6 class="card-subtitle mb-3"><a href="javascript:void(0)" class="card-link">${post.user.name}</a></h6>
                                    <p class="card-text">${post.description}</p>
                                    <h6 class="card-subtitle text-muted">${post.created_at}</h6>
                                </div>
                            </div>`
                        );
                    });

                    getPostUrl = responseData.links.next;

                    if  (initial === true) {
                        $('#dashboard').append(
                            `<div class="my-5 text-center" id="load-more">
                            <button class="btn btn-primary" onclick="loadPosts()">Load More</button>
                        </div>`
                        );
                        initial = false;
                    }
                },
                error: function (data) {
                    console.log(data)
                }
            });
        }
    </script>
@endsection
