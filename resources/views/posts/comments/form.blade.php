<div class="mb-3">
    @if($commentId)
        <p class="btn-link reply_form_button_{{ $commentId }}"
           onclick="showReplyForm('{{ $commentId }}')">Add reply</p>
    @endif
    <div class="{{ $commentId ? 'd-none' : '' }} reply_form_{{ $commentId }}">
        <form action="{{ route('comments.store', ['post' => $post->id]) }}" method="post">
            @csrf
            <input type="hidden" name="parent_id" value="{{ $commentId }}">
            <textarea name="comment" class="form-control mb-1" id="" placeholder="Add reply"
                      rows="2" required></textarea>
            <button type="submit" class="btn btn-primary btn-sm">Reply</button>
        </form>
    </div>
</div>