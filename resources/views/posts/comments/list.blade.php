@foreach($commentGroup as $comment)
    <div class="{{ $nested ? 'ml-4' : '' }}">
        <hr>
        <div class="mb-1">
            <p class="card-text">
                <a href="#" class="card-link mr-1">{{ $comment->user->name }}</a> {{ $comment->comment}}
            </p>
        </div>
        @include('posts.comments.form', ['commentId' => $comment->id])

        @isset($comments[$comment->id])
            @include('posts.comments.list', ['commentGroup' => $comments[$comment->id], 'nested' => true])
        @endisset
    </div>
@endforeach