@extends('layouts.app')

@section('title', 'Create Post')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create Post</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('posts.store') }}"
                              enctype="multipart/form-data">
                            @csrf

                            <div class="form-group row">
                                <label for="title" class="col-md-4 col-form-label text-md-right">Title {!! mandatory() !!}</label>

                                <div class="col-md-6">
                                    <input id="title" type="title"
                                           class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}"
                                           name="title" value="{{ old('title') }}" required autofocus>

                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description"
                                       class="col-md-4 col-form-label text-md-right">Description {!! mandatory() !!}</label>

                                <div class="col-md-6">
                                    <textarea id="description" type="description"
                                              class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
                                              name="description" required>{{ old('description') }}</textarea>

                                    @if ($errors->has('description'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="banner_image"
                                       class="col-md-4 col-form-label text-md-right">Banner Image {!! mandatory() !!}</label>

                                <div class="col-md-6">
                                    <div class="custom-file">
                                        <input type="file" id="banner-image"
                                               class="custom-file-input form-control{{ $errors->has('banner_image') ? ' is-invalid' : '' }}"
                                               name="banner_image" required>
                                        <label class="custom-file-label" for="banner-image">Choose file</label>
                                        @if ($errors->has('banner_image'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('banner_image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#banner-image").change(function (e) {
            var $this = $(this);
            $this.next().html($this.val().split('\\').pop());
        });
    </script>
@endsection