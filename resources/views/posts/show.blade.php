@extends('layouts.app')

@section('title', 'Feeds')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8" id="dashboard">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <div id="feeds">
                    <div class="card">
                        <img class="card-img-top" src="{{ $post->bannerImageUrl() }}" alt="Post banner image">
                        <div class="card-body">
                            <div class="mb-1" id="post-content">
                                <h5 class="card-title">{{ $post->title }}</h5>
                                <h6 class="card-subtitle mb-3"><a href="javascript:void(0)" class="card-link">{{ $post->user->name }}</a>
                                    <span class="card-subtitle text-muted">{{ $post->created_at->diffForHumans() }}</span>
                                </h6>
                                <p class="card-text">{{ $post->description }}</p>
                            </div>
                            @include('posts.comments.form', ['commentId' => null])
                            @include('posts.comments.list', ['commentGroup' => $comments['base'], 'nested' => false])
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        function showReplyForm(commentId) {
            $('.reply_form_'+commentId).removeClass('d-none');
            $('.reply_form_button_'+commentId).addClass('d-none');
        }
    </script>
@endsection
